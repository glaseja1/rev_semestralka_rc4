﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace REV_sem1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            byte[] encr = File.ReadAllBytes(@"passwordHashRC4 glaseja1.bin");
            byte[] key = System.Text.Encoding.Default.GetBytes("uBOWzpxInvB9VER");

            //for adameste pass length 24
            encr = File.ReadAllBytes(@"passwordHashRC4 adameste.bin");
            key = System.Text.Encoding.Default.GetBytes("5XsZEnxUYX97VER");

           /* //for cudkajak
            key = System.Text.Encoding.Default.GetBytes("mzSe29I61kYOp");
            encr = File.ReadAllBytes(@"passwordHashRC4 cudkajak.bin");
            */
           /* //for brejnjan
            key = System.Text.Encoding.Default.GetBytes("zhoK1H8kHh64b");
            encr = File.ReadAllBytes(@"passwordHashRC4 brejnjan.bin");
            */
            //for cerveka2, pass length 21        password is VVp1938fzjZMfcuVYdb8Z
            /*key = System.Text.Encoding.Default.GetBytes("SlGKIaNFk5nnvq6");
            encr = File.ReadAllBytes(@"passwordHashRC4 cerveka2.bin");
            //encr = System.Text.Encoding.Default.GetBytes("uBOWzpxInvB9VER");

            /*encr = RC4.Encrypt(key, encr);
            MessageBox.Show(System.Text.Encoding.Default.GetString(encr));
            MessageBox.Show(Base64Encode(System.Text.Encoding.Default.GetString(encr)));
           */
         
            //for tomasmax pass length 21
            key = System.Text.Encoding.Default.GetBytes("GG1IejhmtUKN0MQ");
            encr = File.ReadAllBytes(@"passwordHashRC4 tomasmax.bin");
            

            this.rc4(ref encr, key);
            string pass = System.Text.Encoding.Default.GetString(encr).Substring(0, (int)numericUpDown1.Value);
            textBox1.Text = pass;
            MessageBox.Show(pass);
        }

        public void rc4(ref Byte[] bytes, Byte[] key)
        {
            Byte[] s = new Byte[256];
            Byte[] k = new Byte[256];
            Byte temp;
            int i, j;

            for (i = 0; i < 256; i++)
            {
                s[i] = (Byte)i;
                k[i] = key[i % key.GetLength(0)];
            }
            //create the table
            j = 0;
            for (i = 0; i < 256; i++)
            {
                j = (j + s[i] + k[i]) % 256;
                temp = s[i];
                s[i] = s[j];
                s[j] = temp;
            }
            //here s is the keystream?
            
            //pseudo random generation PRGA
            i = j = 0;
            for (int x = 0; x < bytes.GetLength(0); x++)
            {
                i = (i + 1) % 256;
                j = (j + s[i]) % 256;
                temp = s[i];
                s[i] = s[j];
                s[j] = temp;
                int t = (s[i] + s[j]) % 256;
                //xor with the keystream
                bytes[x] ^= s[t];

                //46 xor X = 115
                //189 xor 241 = 
            }
        }
    }
}
