#include <openssl/evp.h>
#include <openssl/rc4.h>

void loadBytesFromFile(const char* finalename, char** buff, int* len)
{
  FILE* f;
  f = fopen(finalename, "rb");
  int red = 0;

  *buff = (char*)malloc(*len + 2);
  *len = fread(*buff, 1, *len, f);
}
int main()
{
  RC4_KEY key;

  //the rc4 key
  char* rc4KeyBytes = "uBOWzpxInvB9VER";

  char* output = malloc(500);
  char* rc4Input = NULL;

  int rc4InputLength = 23;
  //the rc4 hash
  loadBytesFromFile("passwordHashRC4 glaseja1.bin", &rc4Input, &rc4InputLength);

  //uncomment this to do it for student cerveka2 for whom it fucking works
  //loadBytesFromFile("passwordHashRC4 cerveka2.bin", &rc4Input, &rc4InputLength);
  //rc4KeyBytes = "SlGKIaNFk5nnvq6";

  printf("rc4 key: %s\n", rc4KeyBytes);
  printf("red rc4 input: %s (length %d)\n\n", rc4Input, rc4InputLength);


  printf("encrypting: \n");
  printf("%s\n", rc4Input);

  RC4_set_key(&key, 15, rc4KeyBytes);
  RC4(&key, rc4InputLength, rc4Input, output);

  //after xoring with keystream
  printf("output: \n", output);
  printf("%s\n", output);
  return 0;
}


/*
root@jogurt:~/Desktop/FIT/magistr/REV/semestral# gcc main.c -lcrypto && ./a.out
rc4 key: uBOWzpxInvB9VER
red rc4 input: ��a�X��1XT����4r�	�*�t (length 23)

encrypting: ��a�X��1XT����4r�	�*�t

output:
�:b�����S���e!��|͓�
 */
